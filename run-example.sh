#!/bin/bash

cc -o diff diff.c || exit 1
cc -o patch patch.c || exit 1
echo "#" ./diff a/patch.c b/patch.c ">" patch.diff
./diff a/* b/* > patch.diff || exit 1
echo "#" cat patch.diff
cat patch.diff || exit 1
echo "#" cd dir1
cd dir1 || exit 1
echo "#" ../patch -p1 "<" ../patch.diff
../patch -p1 < ../patch.diff || exit 1
cd .. || exit 1
rm -f diff patch patch.diff
