#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

void vfatalf(const char* format, va_list arg) {
	const char prefix[] = { 'F', 'A', 'T', 'A', 'L', ':', ' ' };
	size_t flen = strlen(format) + 1;
	char real_format[sizeof(prefix) + flen];
	memcpy(real_format, prefix, sizeof(prefix));
	memcpy(real_format + sizeof(prefix), format, flen);
	vfprintf(stderr, real_format, arg);
	abort();
}

void fatalf(const char* format, ...) {
	va_list arg;
	va_start(arg, format);
	vfatalf(format, arg);
}

char *strdup_filename (const char *s, int p1) {
	if (*s == '\t')
		return strdup ("");
	const char *p = s+1;
	while ((p = strchr (p, '\t')) && p[-1] == '\\');
	while (p1 --> 0) {
		s = p ? memchr(s, '/', p-s) : strchr(s, '/');
		if (!s++)
			return strdup("");
	}
	char* ret = p ? strndup (s, p-s) : strdup(s);
	char* e = strchr (ret, '\n');
	if (e)
		*e = 0;
	return ret;
}

int main (int argc, char** argv) {
	int argf = 1;
	int p1 = 1;
	if (argc > argf && argv[argf][0] == '-' && argv[argf][1] == 'p') {
		p1 = atoi(argv[argf++]+2);
	}
	FILE *patchfile = argc > argf ? fopen(argv[argf], "rb") : stdin;
	FILE *oldf = NULL;
	FILE *tmpf = NULL;
	char *oldname = NULL;
	char *newname = NULL;
	int oldline_id = 0;
	int newline_id = 0;
	int oldline, oldlen = 0, newline, newlen = 0;
	for (;;) {
		char line[640];
		int eof = !fgets (line, 640, patchfile);
		if (eof || !oldlen && !newlen && !strncmp (line, "--- ", 4)) {
			if (oldlen || newlen) {
				fatalf ("Unexpected end of input\n");
			}
			if (tmpf) {
				char buf[2048];
				while (fgets (buf, 2048, oldf))
					fputs (buf, tmpf);
				fclose (oldf);
				fseek (tmpf, 0, SEEK_SET);
				FILE *newf = fopen (newname, "wb");
				while (fgets (buf, 2048, tmpf))
					fputs (buf, newf);
				free (oldname);
				free (newname);
				fclose (tmpf);
				fclose (newf);
			}
			if (eof)
				break;
			oldname = strdup_filename (line + 4, p1);
			if (!fgets (line, 640, patchfile))
				fatalf ("Unexpected end of input\n");
			if (strncmp (line, "+++ ", 4)) {
				fatalf ("Expected filepath, got '%s'\n", line);
			}
			newname = strdup_filename (line + 4, p1);
			oldf = fopen (oldname, "rb");
			if (!oldf)
				fatalf ("File '%s' does not exist\n", oldname);
			tmpf = tmpfile ();
			oldline_id = 1;
			newline_id = 1;
		}
		else if (tmpf && !strncmp (line, "@@ -", 4)) {
			if (oldlen || newlen) {
				fatalf ("Unexpected '@@'\n");
			}
			if (sscanf (line, "@@ -%d,%d +%d,%d @@", &oldline, &oldlen, &newline, &newlen) != 4) {
				oldline = 1;
				newline = 1;
				if (sscanf (line, "@@ -%d +%d @@", &oldlen, &newlen) != 2)
					if (sscanf (line, "@@ -%d,%d +%d @@", &oldline, &oldlen, &newlen) != 3)
						if (sscanf (line, "@@ -%d +%d,%d @@", &oldlen, &newline, &newlen) != 3)
							fatalf ("Cannot parse '%s'\n", line);
			}
		}
		else if (oldlen || newlen) {
			if (!strchr (" +-", *line)) {
				fatalf ("Unexpected start of line '%c'\n", *line);
			}
			if (oldline_id == 1 && oldline == 0 && oldlen == 0)
				oldline_id--;
			if (oldline_id > oldline || newline_id > newline) {
				fatalf ("Corruption\n");
			}
			char buf[639];
			if (oldline_id < oldline) {
				if (*line == '+') {
					fatalf ("Expected context lines\n");
				}
				while (oldline_id < oldline) {
					if (!fgets (buf, 639, oldf)) {
						fatalf ("Unexpected end of original file\n");
					}
					fputs (buf, tmpf);
					oldline_id++;
					newline_id++;
				}
				for (;;) {
					if (!fgets (buf, 639, oldf)) {
						fatalf ("Unexpected end of original file\n");
					}
					if (!strcmp (buf, line + 1))
						break;
					fputs (buf, tmpf);
					oldline_id++;
					oldline++;
					newline_id++;
					newline++;
				}
				if (--oldlen < 0) {
					fatalf ("Corrupt length\n");
				}
			}
			else if (*line == '+') {
				strcpy (buf, line+1);
			}
			else if (--oldlen < 0) {
				fatalf ("Corrupt length\n");
			}
			else if (!fgets (buf, 639, oldf)) {
				fatalf ("Unexpected end of original file\n");
			}
			else if (strcmp (buf, line + 1)) {
				if (*buf && buf[strlen(buf+1)] == '\n' && line[strlen(line+1)] == '\n')
					buf[strlen(buf+1)] = line[strlen(line+1)] = 0;
				fatalf ("Mismatch between lines '%s' and '%s'\n", buf, line + 1);
			}
			else {
				oldline_id++;
				oldline++;
			}
			if (*line != '-') {
				if (--newlen < 0) {
					fatalf ("Corrupt length\n");
				}
				fputs (buf, tmpf);
				newline_id++;
				newline++;
			}
		}
	}
}
