#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

char *asreprintf (char *format, char *dest, const char *src) {
	char *r;
	if (asprintf (&r, format, dest, src) < 0)
		abort ();
	free (dest);
	return r;
}

char **lcs_internal (char **a, int a_length, char **b, int b_length, int start_index) {
	if (a_length > 256) {
		int t1 = 0, t2 = 0;
		int acut = a_length/2;
		int *tmp1 = malloc (b_length * sizeof *tmp1);
		int *tmp2 = malloc (b_length * sizeof *tmp2);
		for (int i = 0; i < acut; i++)
			for (int j = 0; j < b_length; j++) {
				int t = t1;
				t1 = tmp1[j];
				if (!strcmp (a[i], b[j]))
					tmp1[j] = (i == 0 || j == 0) ? 1 : (t + 1);
				else if (i == 0 || (j != 0 && tmp1[j] < tmp1[j-1]))
					tmp1[j] = j ? tmp1[j-1] : 0;
			}
		for (int i = a_length; i > acut; i--)
			for (int j = b_length; j > 0; j--) {
				int t = t2;
				t2 = tmp2[j-1];
				if (!strcmp (a[i-1], b[j-1]))
					tmp2[j-1] = (i == a_length || j == a_length) ? 1 : (t + 1);
				else if (i == 0 || (j != 0 && tmp2[j-1] < tmp2[j]))
					tmp2[j-1] = j ? tmp2[j] : 0;
			}
		int max = -1;
		for (int i = 0; i < b_length; i++)
			if ((max == -1 ? 0 : tmp1[max]) + tmp2[max+1] < tmp1[i] + (i+1 == b_length ? 0 : tmp2[i]))
				max = i;
		char **lcs1 = lcs_internal (a, acut, b, max+1, start_index);
		char **lcs2 = lcs_internal (a+acut, a_length-acut, b+max+1, b_length-max-1, start_index+acut);
		char **r = malloc (((size_t)*lcs1 + (size_t)*lcs2 + 1) * sizeof *r);
		*r = (char*)((size_t)*lcs1 + (size_t)*lcs2);
		memcpy (r+1, lcs1+1, (size_t)*lcs1 * sizeof *r);
		free (lcs1);
		memcpy (r+1+(size_t)*lcs1, lcs2+1, (size_t)*lcs2 * sizeof *r);
		free (lcs2);
		return r;
	}
	int8_t *tmp1 = malloc (a_length * b_length * sizeof *tmp1);
	size_t *tmp2 = malloc (a_length * b_length * sizeof *tmp2);
	for (int i = 0; i < a_length; i++)
		for (int j = 0; j < b_length; j++) {
			if (!strcmp (a[i], b[j]))
				tmp1[i*b_length+j] = 0;
			else if (i == 0 || j == 0)
				tmp1[i*b_length+j] = i == 0 ? 1 : 2;
			else
				tmp1[i*b_length+j] = tmp2[(i-1)*b_length+j] < tmp2[i*b_length+j-1] ? 1 : 2;
			switch (tmp1[i*b_length+j]) {
				case 0:
					tmp2[i*b_length+j] = (i == 0 || j == 0) ? 1 : (tmp2[(i-1)*b_length+j-1] + 1);
					break;
				case 1:
					tmp2[i*b_length+j] = j ? tmp2[i*b_length+j-1] : 0;
					break;
				case 2:
					tmp2[i*b_length+j] = tmp2[(i-1)*b_length+j];
					break;
			}
		}
	size_t j = a_length*b_length-1;
	size_t i = tmp2[j]*2;
	char **r = malloc ((i+1) * sizeof *r);
	*r = (char*)i;
	while (i)
		switch (tmp1[j]) {
			case 0:
				r[i--] = b[j%b_length];
				r[i--] = a[j/b_length];
				j -= b_length+1;
				break;
			case 1:
				j -= 1;
				break;
			case 2:
				j -= b_length;
				break;
		}
	free (tmp1);
	free (tmp2);
	return r;
}

char **lcs (char **a, char **b) {
	int a_length = 0, b_length = 0;
	while (a[a_length]) a_length++;
	while (b[b_length]) b_length++;
	lcs_internal (a, a_length, b, b_length, 0);
}

int main (int argc, char **argv) {
	FILE *f1 = fopen (argv[1], "rb");
	FILE *f2 = fopen (argv[2], "rb");
	char **a = calloc (1, sizeof *a);
	char **b = calloc (1, sizeof *b);
	int i = 0, j = 0;
	char buf[639];
	while (fgets (buf, 639, f1)) {
		a[i++] = strdup(buf);
		if (!(i & (i+1))) {
			a = realloc (a, 2 * (i+1) * sizeof *a);
			a[i+1] = NULL;
		}
	}
	while (fgets (buf, 639, f2)) {
		b[j++] = strdup(buf);
		if (!(j & (j+1))) {
			b = realloc (b, 2 * (j+1) * sizeof *b);
			b[j+1] = NULL;
		}
	}
	char **cs = lcs (a, b);
	size_t csl = (size_t)*cs;
	i = j = 0;
	char *s = calloc (1, 1);
	int beg1 = 0;
	int beg2 = 0;
	int linec1 = 0;
	int linec2 = 0;
	int ci = 0;
	printf ("--- %s\n+++ %s\n", argv[1], argv[2]);
	while (a[i] && b[j]) {
		int fast_forward = 1;
		for (int k = 0; k < 7 && a[i+k]; k++)
			if (a[i+k] != cs[2*(ci+k)+1])
				fast_forward = 0;
		for (int k = 0; k < 7 && b[j+k]; k++)
			if (b[j+k] != cs[2*(ci+k)+2])
				fast_forward = 0;
		if (fast_forward || (!i && !j)) {
			if (linec1 || linec2) {
				for (int k = 0; k < 3 && a[i+k]; k++)
					s = strcat(strcat(realloc (s, strlen (s) + strlen (a[i+k])+1), " "), a[i+k]);
				i += 3;
				j += 3;
				ci += 3;
				linec1 += 3;
				linec2 += 3;
				printf ("@@ -%d,%d +%d,%d @@\n%s", beg1, linec1, beg2, linec2, s);
				*s = 0;
			}
			while (a[i] && b[j] && ci <= csl/2 && a[i] == cs[2*ci+1] && b[j] == cs[2*ci+2]) {
				i++;
				j++;
				ci++;
			}
			if (!a[i] && !b[j])
				break;
			beg1 = i < 3 ? 1 : i-2;
			beg2 = j < 3 ? 1 : j-2;
			if (i - beg1 != j - beg2)
				abort();
			linec1 = linec2 = 0;
			if (i < 0)
				i = 0;
			if (j < 0)
				j = 0;
			linec1 = 0;
			linec2 = 0;
			for (int k = beg1-1; k < i; k++) {
				s = asreprintf("%s %s", s, a[k]);
				linec1++;
				linec2++;
			}
		}
		while (a[i] && b[j] && ci <= csl/2 && a[i] == cs[2*ci+1] && b[j] == cs[2*ci+2]) {
			s = asreprintf("%s %s", s, a[i]);
			i++;
			j++;
			ci++;
			linec1++;
			linec2++;
		}
		while (a[i] && ci <= csl/2 && a[i] != cs[2*ci+1]) {
			s = asreprintf("%s-%s", s, a[i]);
			i++;
			linec1++;
		}
		while (b[j] && ci <= csl/2 && b[j] != cs[2*ci+2]) {
			s = asreprintf("%s+%s", s, b[j]);
			j++;
			linec2++;
		}
	}
	if (*s)
		printf ("@@ -%d,%d +%d,%d @@\n%s", beg1, linec1, beg2, linec2, s);
	free (a);
	free (b);
	free (cs);
}
